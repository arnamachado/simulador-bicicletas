#!/bin/python3

import xml.etree.ElementTree as ET

def there_is_cycle(l, d):
    for each in l:
        if d[each]:
            return True

    return False

event_file = 'clean_events.xml'

tree = ET.parse(event_file)

root = tree.getroot()

trips = {}

for event in root.findall('event'):
    #if event.attrib['type'] == 'entered link':
    if True:
        try:
            if event.attrib['person'] in trips:
                trips[event.attrib['person']].add(int(event.attrib['link']))
            else:
                trips[event.attrib['person']] = set([int(event.attrib['link'])])
        except KeyError:
            pass

network_file = 'network.xml'

net_tree = ET.parse(network_file)

net_root = net_tree.getroot()

is_cycleway_link = {}
is_cyclelane_link = {}

for link in net_root.find('links').findall('link'):
    link_id = int(link.attrib['id'])

    if 'cyclelane' in link.attrib:
        is_cyclelane_link[link_id] = bool(link.attrib['cyclelane'])
    else:
        is_cyclelane_link[link_id] = False

    if 'cycleway' in link.attrib:
        is_cycleway_link[link_id] = bool(link.attrib['cycleway'])
    else:
        is_cycleway_link[link_id] = False

trips_in_cyclelane = 0
trips_in_cycleway = 0
trips_in_cyclestructure = 0

for trip in trips:
    there_is_cyclestructure = False
    if there_is_cycle(trips[trip], is_cycleway_link):
        there_is_cyclestructure = True
        trips_in_cycleway += 1

    if there_is_cycle(trips[trip], is_cyclelane_link):
        there_is_cyclestructure = True
        trips_in_cyclelane += 1

    if there_is_cyclestructure:
        trips_in_cyclestructure += 1

print('cyclelane trips:', trips_in_cyclelane)
print('cycleway trips:', trips_in_cycleway)
print('cyclestructure trips:', trips_in_cyclestructure)
print('total trips:', len(trips))
