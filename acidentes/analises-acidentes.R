# Instalação de pacotes necessários
# install.packages("tydiverse")
# install.packages("purrr")
# install.packages("lubridate")
# install.packages("foreign")
# install.packages("zoo")


# Carregamento de pacotes necessários
library(tidyverse)
library(purrr)
library(lubridate)
library(foreign)
library(zoo)

##### Marcação de quais acidentes tiveram como vítimas pedestres ou ciclistas #####

# Importar dados de acidentes, veiculos e vitimas de jan/2016 a jun/2019
df_incidentes_total <- read.csv(file = "./acidentes/dados-acidentes-01-2016-07-2019.csv")
df_veiculos_total <- read.csv(file = "./acidentes/dados-acidentes-veiculos-01-2016-07-2019.csv")
df_vitimas_total <- read.csv(file = "./acidentes/dados-acidentes-vitimas-01-2016-07-2019.csv")

# Excluir variaveis que não são de interesse
df_incidentes_total[c("Acidente", "acidente_id", "occurred_from")] <- NULL
df_veiculos_total[c("veiculo_id")] <- NULL
df_vitimas_total[c("veiculo")] <- NULL

# Unir base de dados de incidentes com a de veiculos e da de vitimas
df_incidentes_com_veiculos <- nest_join(df_incidentes_total, df_veiculos_total, by = "record_id", name = "veiculo")
df_incidentes_com_veiculos_vitimas <- nest_join(df_incidentes_com_veiculos, df_vitimas_total, by = "record_id", name = "vitima")

# Criar variaveis de indicam se bicicletas e se pedestres estiveram envolvidos nos acidentes
df_incidentes_com_veiculos_vitimas$bicicleta_envolvida <- NA
df_incidentes_com_veiculos_vitimas$pedestre_envolvido <- NA

# Extrair a informação de se um acidente envolveu bicicleta
df_incidentes_com_veiculos_vitimas$temp <- df_incidentes_com_veiculos_vitimas$veiculo %>% map(~ "Bicicleta" %in% .x[[2]])
df_incidentes_com_veiculos_vitimas$bicicleta_envolvida <- df_incidentes_com_veiculos_vitimas$temp == TRUE

# Extrair a informação de se um acidente envolveu pedestre
df_incidentes_com_veiculos_vitimas$temp <- df_incidentes_com_veiculos_vitimas$vitima%>% map(~ "Pedestre" %in% .x[[1]])
df_incidentes_com_veiculos_vitimas$pedestre_envolvido <- df_incidentes_com_veiculos_vitimas$temp == TRUE

# Excluir variavel temporaria
df_incidentes_com_veiculos_vitimas$temp <- NULL
df_incidentes_com_veiculos_vitimas$veiculo <- NULL
df_incidentes_com_veiculos_vitimas$vitima <- NULL
df_incidentes_com_veiculos_vitimas$incidente_details_id <- NULL

# Exportando dados de acidentes
write.csv(df_incidentes_com_veiculos_vitimas, file = "df_incidentes_2016-2019.csv")

# Exportando dados de acidentes de 2017
df_incidentes_2017 <- df_incidentes_com_veiculos_vitimas %>% filter(ano_ocorrencia == 2017)
write.csv(df_incidentes_2017, file = "df_incidentes_2017.csv")

##### Marcação de quais acidentes ocorreram na presença de infraestrutura cicloviaria #####

##### Cálculo das quantidades de acidentes #####

# Importar dados de acidentes de 2017, com marcação de infraestrutura cicloviaria e o dataset de vitimas
df_incidentes_2017_infra <- read.csv(file = "./acidentes/df_incidentes_2017_infra.csv")
df_vitimas_total <- read.csv(file = "./acidentes/dados-acidentes-vitimas-01-2016-07-2019.csv")

# Unindo as informações de acidentes na base de vítimas
df_vitimas_2017_infra <- left_join(df_vitimas_total, df_incidentes_2017_infra, by = "record_id") %>% 
  filter(ano_ocorrencia == 2017)

# Definição do dia da semana em que os acidentes ocorreram (1 - domingo, 2 - segunda, etc.)
df_vitimas_2017_infra <- df_vitimas_2017_infra %>%
  mutate(mes = sprintf("%02d",mes_ocorrencia)) %>% 
  mutate(dia = sprintf("%02d",dia_ocorrencia)) %>% 
  mutate(data = paste0(ano_ocorrencia, mes, dia)) %>% 
  mutate(data = ymd(data)) %>% 
  mutate(dia_semana = wday(data))

# Seleção do dataset que contenha apenas ocorrencias de dia de semana
df_vitimas_2017_infra_dia_semana <- df_vitimas_2017_infra %>%
  filter(dia_semana %in% c(2:6))

# Cálculo da quantidade de vítimas de acidentes por hora (total) - dia de semana (365-105=260 dias uteis)
df_vitimas_2017_dia_semana_total <- df_vitimas_2017_infra_dia_semana %>%
  group_by(hora_ocorrencia) %>%
  add_tally(name = "qtde_vitimas_hora") %>%
  distinct(qtde_vitimas_hora) %>% 
  mutate (qtde_vitimas_hora = qtde_vitimas_hora/260)

# Cálculo da quantidade (média) de vítimas de acidentes por hora (com infra cicloviaria) - dia de semana
df_vitimas_2017_dia_semana_com_infra <- df_vitimas_2017_infra_dia_semana %>%
  filter(infra_tipo != 0) %>% 
  group_by(hora_ocorrencia) %>% 
  add_tally(name = "qtde_vitimas_hora") %>%
  distinct(qtde_vitimas_hora)%>% 
  mutate (qtde_vitimas_hora = qtde_vitimas_hora/260)

# Cálculo da quantidade (média) de vítimas de acidentes por hora (sem infra cicloviaria) - dia de semana
df_vitimas_2017_dia_semana_sem_infra <- df_vitimas_2017_infra_dia_semana %>%
  filter(infra_tipo == 0) %>% 
  group_by(hora_ocorrencia) %>% 
  add_tally(name = "qtde_vitimas_hora") %>%
  distinct(qtde_vitimas_hora)%>% 
  mutate (qtde_vitimas_hora = qtde_vitimas_hora/260)

##### Cálculo das quantidades de viagens #####

# Leitura dos dados da OD2017
df_od2017 <- read.dbf("./acidentes/OD_2017.dbf")

# Criação de variável horária contínua (ao longo do dia)
df_od2017 <- df_od2017 %>% 
  mutate(min_dia_saida = (H_SAIDA*60) + MIN_SAIDA) %>% 
  mutate(min_dia_chegada = (H_CHEG*60) + MIN_CHEG)

# Contagem de viagens por hora de saída ao longo do dia
df_od2017_qtde_viagens_saida <- df_od2017 %>%
  filter(! MODOPRIN %in% c(1,2,3)) %>% 
  filter(!is.na(H_SAIDA)) %>% 
  group_by(H_SAIDA) %>% 
  add_tally(wt = FE_VIA, name = "n") %>%
  distinct(n)

##### Cálculo da probabilidade de acidentes #####

# Formação de banco de dados único
df_2017 <- df_od2017_qtde_viagens_saida %>% 
  left_join(df_vitimas_2017_dia_semana_total, by = c("H_SAIDA" = "hora_ocorrencia")) %>% 
  left_join(df_vitimas_2017_dia_semana_com_infra, by = c("H_SAIDA" = "hora_ocorrencia"), suffix = c("_total", "_com_infra")) %>% 
  left_join(df_vitimas_2017_dia_semana_sem_infra, by = c("H_SAIDA" = "hora_ocorrencia")) %>% 
  rename(qtde_vitimas_hora_sem_infra = qtde_vitimas_hora, qtde_total_viagens = n, hora = H_SAIDA) %>% 
  arrange(hora)

# Se houver algum NA, ele atribui a média dos horários adjacentes
df_2017$qtde_vitimas_hora_total <- na.approx(df_2017$qtde_vitimas_hora_total)
df_2017$qtde_vitimas_hora_com_infra <- na.approx(df_2017$qtde_vitimas_hora_com_infra)
df_2017$qtde_vitimas_hora_sem_infra <- na.approx(df_2017$qtde_vitimas_hora_sem_infra)

# Probabilidade de acidente total
df_2017 <- df_2017 %>% 
  mutate(prob_acidente_total = qtde_vitimas_hora_total / qtde_total_viagens)

# Há 504km de vias con tratamento cicloviário permanente (473,7km de Ciclovias/Ciclofaixas e 30,3km de ciclorrotas)
# Fonte: CET http://www.cetsp.com.br/consultas/bicicleta/mapa-de-infraestrutura-cicloviaria.aspx
# Há 17,2mil km de via pavimentas em São Paulo
# Fonte: CET https://32xsp.org.br/especial/ruas-sem-asfalto/

# Cálculo de quantas viagens ocorreram na presença de infraestrutura ciclavel
prop_vias_com_infra_ciclavel <- 504/17200

## Atenção! Adotar tal proporação considera que todas as vias sao homogeneas (capacidade, ocupação, etc.)
## Ao fazer isto deste modo, também considera-se homogeneidade ao longo do dia

# Probabilidade de acidente com infra
df_2017 <- df_2017 %>% 
  mutate(prob_acidente_com_infra = qtde_vitimas_hora_com_infra / (qtde_total_viagens * prop_vias_com_infra_ciclavel))

# Probabilidade de acidente sem infra
df_2017 <- df_2017 %>% 
  mutate(prob_acidente_sem_infra = qtde_vitimas_hora_sem_infra / (qtde_total_viagens-(qtde_total_viagens * prop_vias_com_infra_ciclavel)))

# Gerando csv com probabilidades
write.csv(df_2017, "./acidentes/df_2017_prob_acidentes_dia_semana.csv")
