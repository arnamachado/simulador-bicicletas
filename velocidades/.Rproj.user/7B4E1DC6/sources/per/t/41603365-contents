# Instalação de pacotes necessários
install.packages("tydiverse")
install.packages("purrr")
install.packages("foreign")
install.packages("lubridate")
install.packages("crayon")
install.packages("gamlss")
install.packages("gamlss.dist")
install.packages("gamlss.add")
install.packages("rmutil")

# Carregamento de pacotes necessários
library(purrr)
library(foreign)
library(lubridate)
library(crayon)
library(gamlss)
library(gamlss.dist)
library(gamlss.add)
library(tidyverse)
library(rmutil)

# Leitura dos dados da OD2017
df_od2017 <- read.dbf("./Pesquisa-Origem-Destino-2017-Banco-Dados/OD 2017/Banco de dados/OD_2017.dbf")
  
# Cálculo das distancias [km], durações [h] e velocidades [km/h] das viagens, excluindo quem nao viajou (duração nula)
df_od2017 <- df_od2017 %>%
    filter(!is.na(DURACAO)) %>% 
    mutate(dist_euclidiana_km = (sqrt(((CO_O_X-CO_D_X)^2)+((CO_O_Y-CO_D_Y)^2)))/1000) %>% 
    mutate(duracao_h = DURACAO/60) %>% 
    mutate(velocidade_km_h = dist_euclidiana_km / duracao_h)
  
# Criação de variável horária contínua (ao longo do dia)
df_od2017 <- df_od2017 %>% 
    mutate(min_dia_saida = (H_SAIDA*60) + MIN_SAIDA) %>% 
    mutate(min_dia_chegada = (H_CHEG*60) + MIN_CHEG)

# Eliminação de outliers (geral)
boxplot(df_od2017$velocidade_km_h) # visualização da dispersão dos valores
stats_boxplot <- boxplot.stats(df_od2017$velocidade_km_h) # geração das estatísticas do boxplot
df_od2017 <- df_od2017 %>%
  filter(velocidade_km_h < stats_boxplot$stats[5])

# Eliminação de outliers das bibicletas
df_od2017_bike <- df_od2017 %>% filter (MODOPRIN == 15)
boxplot(df_od2017_bike$velocidade_km_h) # visualização da dispersão dos valores
stats_boxplot_bike <- boxplot.stats(df_od2017_bike$velocidade_km_h) # geração das estatísticas do boxplot
df_od2017_bike <- df_od2017_bike %>%
  filter(velocidade_km_h < stats_boxplot_bike$stats[5])

# Cálculo das velocidades médias por hora de saída ao longo do dia
df_od2017_vel_medias_saida <- df_od2017_bike %>% 
  group_by(MODOPRIN, H_SAIDA) %>% 
  summarize(velocidade_media_horaria_saida = weighted.mean(velocidade_km_h, w = FE_VIA))

# Contagem de viagens de bike por hora de saída ao longo do dia
df_od2017_qtde_viagens_saida <- df_od2017_bike %>%
  group_by(H_SAIDA) %>% 
  add_tally(wt = FE_VIA, name = "n") %>%
  distinct(n)

# Checagem do total de viagens
total_n <- sum(df_od2017_qtde_viagens_saida$n)

# Cálculo das velocidades médias por hora de chegada ao longo do dia
df_od2017_vel_medias_chegada <- df_od2017_bike %>% 
  group_by(MODOPRIN, H_CHEG) %>% 
  summarize(velocidade_media_horaria_chegada = weighted.mean(velocidade_km_h, w = FE_VIA))

# Plotagem do histograma de velocidades (geral) considerando os fatores de expansão de viagem
df_od2017 %>% 
  ggplot(aes(velocidade_km_h, weight = FE_VIA)) + 
  geom_histogram()

# Plotagem do histograma de velocidades (por modo) considerando os fatores de expansão de viagem
df_od2017 %>% 
  ggplot(aes(velocidade_km_h, weight = FE_VIA)) + 
  geom_histogram() +
  facet_grid(cols = vars(MODOPRIN))

# Plotagem do histograma de velocidades (bicicletas) considerando os fatores de expansão de viagem
df_od2017_bike %>% 
  ggplot(aes(velocidade_km_h, weight = FE_VIA)) + 
  geom_histogram()

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de saída e considerando os fatores de expansão de viagem
df_od2017 %>% filter (MODOPRIN == 15) %>% 
  ggplot(aes(min_dia_saida, velocidade_km_h, weight = FE_VIA)) + 
  geom_point()

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de saída e considerando os fatores de expansão de viagem (heatmap)
df_od2017 %>% filter (MODOPRIN == 15) %>% 
  ggplot(aes(min_dia_saida, velocidade_km_h, weight = FE_VIA)) + 
  geom_point() + 
  stat_density2d(aes(fill=..density..), geom = "tile", contour = FALSE) +
  scale_fill_gradient2(low = "white", high = "red")

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de chegada e considerando os fatores de expansão de viagem
df_od2017 %>% filter (MODOPRIN == 15) %>% 
  ggplot(aes(min_dia_chegada, velocidade_km_h, weight = FE_VIA)) + 
  geom_point()

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de chegada e considerando os fatores de expansão de viagem (heatmap)
df_od2017 %>% filter (MODOPRIN == 15) %>% 
  ggplot(aes(min_dia_chegada, velocidade_km_h, weight = FE_VIA)) + 
  geom_point() + 
  stat_density2d(aes(fill=..density..), geom = "tile", contour = FALSE) +
  scale_fill_gradient2(low = "white", high = "red")

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de chegada e considerando os fatores de expansão de viagem (heatmap)
df_od2017_vel_medias_chegada %>% filter (MODOPRIN == 15) %>% 
  ggplot(aes(H_CHEG, velocidade_media_horaria_chegada)) + 
  geom_line()

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de saida e considerando os fatores de expansão de viagem (heatmap)
df_od2017_vel_medias_saida %>% filter (MODOPRIN == 15) %>% 
  ggplot(aes(H_SAIDA, velocidade_media_horaria_saida)) + 
  geom_line()

# Plotagem do perfil de velocidades (bicicletas) segundo o horário de saida e considerando os fatores de expansão de viagem (heatmap)
df_od2017_qtde_viagens_saida %>%
  ggplot(aes(H_SAIDA, n)) + 
  geom_line()

# Determinação da distribuição que melhor se adequa ao perfil de velocidades de bikes
fit <- fitDist(df_od2017_bike$velocidade_km_h, k = 2, type = "realplus", trace = FALSE, try.gamlss = TRUE)
summary(fit)
# k é penalização do AIC (Akaike Criteria Information) e = 2

# Estaísticas das velodidades
summary(df_od2017_bike$velocidade_km_h)
sd(df_od2017_bike$velocidade_km_h)

# Geração de números aleatórios seguindo essa distribuição
velocidades_distr_gama <- write.csv(as.data.frame(rggamma(n=400000, s =3.204148, m = 7.4410, f = 1.5)), "velocidades_distr_gama.csv")
# hist(aleatorios_gamma$`rggamma(n = 400000, s = 3.204148, m = 7.441, f = 1.5)`)