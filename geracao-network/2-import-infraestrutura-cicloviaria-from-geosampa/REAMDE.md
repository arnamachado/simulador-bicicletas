Sobre shapefiles:

https://wiki.openstreetmap.org/wiki/Shapefiles

Para abrir shapefiles: QGIS

sudo apt-get install qgis

https://www.youtube.com/watch?v=clQzFOGLpM4

Pra abrir no qgis: abrir zip sem o ".cpg"

Duplo cliquei no nome da camada, dá pra ver várias coisas, inclusive nomes dos atributos

Em "Mostrar" dá pra escolher qual campo mostrar no mapa, mas não tá mostrando =(

Cliquei direito no nome da camada > Abrir tabelas de atributos, mostra os dados (.dbf)

Para saber qual ciclovia é cada linha do mapa:

* Exibir paiel "Identificar resultados"
* Clicar em "Abrir automaticamente o formulário"
* Usar ferramenta "identificar feições"
* Clicar na linha desejada que o nome da ciclovia correspondente aparecerá

Lendo shapefiles com Python:

https://agilescientific.com/blog/2017/8/10/x-lines-of-python-read-and-write-a-shapefile

 gdf = gdf.to_crs({'init': 'epsg:26920'}) # converte para uma projeção específica...
 temos que converter pra projeção do simulador

https://shapely.readthedocs.io/en/latest/manual.html

Projeções

http://processamentodigital.com.br/2013/07/27/lista-dos-codigos-epsg-mais-utilizados-no-brasil/

http://geopandas.org/projections.html

Open Street Maps

Dados do Brasil:

https://download.geofabrik.de/south-america/brazil.html

Mapa completo de São Paulo (formato do simulador):

https://raw.githubusercontent.com/ezambomsantana/interscsimulator_scenarios/master/sp_completo/map.xml


