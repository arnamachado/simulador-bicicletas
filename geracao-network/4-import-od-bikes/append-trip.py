#!/bin/python3

import xml.etree.ElementTree as ET

def print_to_file(file_name, string):
    text_file = open(file_name, "w")
    text_file.write(string)
    text_file.close()


def beautifyXML(root_element_object):
    import xml.dom.minidom as minidom
    from io import BytesIO
    buf = BytesIO()
    buf.write(ET.tostring(root_element_object))
    buf.seek(0)
    root = minidom.parse(buf)
    return root.toprettyxml(indent=' '*4)


def main():
    # Simulator trip file containing car, bus, walk, and metro travels, from base-scenario.
    trip_file = 'trips-base.xml' 

    # OD bike trips to be appended.
    trips_bike = 'od-bike-trips-sp-only.xml'

    simulator_tree = ET.parse(trip_file)
    simulator_trip_root = simulator_tree.getroot()

    bikes_tree = ET.parse(trips_bike)
    bikes_tree_root = bikes_tree.getroot()

    for node in bikes_tree_root.findall('trip'):
        simulator_trip_root.append(node)

    trip_xml_string = beautifyXML(simulator_trip_root)
    print_to_file("trips_100d.xml", trip_xml_string)

if __name__== "__main__":
    main()
