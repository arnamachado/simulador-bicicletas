-module(case_test).

-export([main/0]).

func() ->
    Mode = bike,
	case Mode of
		platoon -> 
	 		io:format( "is in platoon" );
	 	_ -> ok % esse ok não é retornado pela função func(); seria o retorno do case, caso o case tivesse sendo atribuído a alguma variável
	end,
    nook.

main() ->
    Ret = func(),
	io:format( "acabou com ~w~n", [Ret] ).
    % main imprime book, e não ok
